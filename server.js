// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var bcrypt = require('bcrypt');

// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

/*app.get("/", function(req, res, next){
	if(req.cookies['token']){
		db.all('SELECT * FROM sessions WHERE token = ?', [req.cookies['token']], function(err, data){
			if(err){
				console.log(err);
			}
			if(data.length == 0){
				res.redirect('/connexion.html');
			}
			else{
				res.redirect('/index.html');
			}
		});
	}
	else{
		res.redirect('connexion.html');
	}
});*/

app.post("/login", function(req, res, next) {
	db.all('SELECT * FROM users WHERE ident=? AND password=?;', [req.body.login, req.body.password], function(err, data){
		if(err){
			console.error(err);
		}
		if(data.length == 0){
			res.json({status : false});
			console.log('wrong password');
		}
		else{
			//res.json({status : true});
			//	Création du token avec comme base un random
			var monToken = createToken(data[0].ident);
			db.all('SELECT token FROM sessions WHERE ident=?;', [req.body.login], function(err, data2){
				if(err){
					console.error(err);
				}
				//	le token existe 
				if(data2.length > 0){
					db.run('UPDATE sessions SET token = ? WHERE ident = ?;', [monToken, req.body.login], function(err, data){
						if(err){
							console.error(err);
						}
						//res.cookie('update', monToken, {expire : new Date() + 1000, path: '/' });
						//res.json({status : true, token : monToken});
						console.log('token exist and update');
					});
				}
				//	le token n'existe pas
				else{
					db.all('INSERT INTO sessions VALUES (?,?);', [req.body.login], [monToken], function(err, data){
						if(err){
							console.error(err);
						}
						//res.cookie('create', monToken, {expire : new Date() + 1000, path: '/' });
						//res.json({status : true, token : monToken});
						console.log('token not exist');
					});
				}
			});
			res.cookie('token', monToken, {expire : new Date() + 1000, path: '/' });
			res.redirect("/connexion.html");
		}
	});
	//res.send("TODO"+ req.body.login);
});

app.get("/disconnect", function(req, res, next){
	res.clearCookie("monToken");
	res.redirect("/");
});

function createToken(id){
	var string = '' + Date.now() + Math.random() + id;
	return bcrypt.hashSync(string, 10);
}

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
